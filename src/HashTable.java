import java.awt.*;
import java.util.LinkedList;
import java.util.Scanner;

public class HashTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        LinkedList<String> list = new LinkedList<String>();
    }

    public static int hashFunc(String s, int length) {
        long hashValue = 0;
        final int p = 31;
        final int m = (int) (1e9 + 9);
        long pPow = 1;
        for (char c : s.toCharArray()) {
            hashValue = (hashValue + (c - 'a' + 1) * pPow)  % m;
            pPow = (pPow * p) % m;
        }
        return (int) (hashValue % length);
    }

    public static void insertHashChain(LinkedList<String> list, String value, int n) {
        int hash = hashFunc(value, n);
        if (list.get(hash) == null) {
            list.add(hash, value);
        } else {
            LinkedList<String> chainNext = new LinkedList<String>();

        }

    }

}