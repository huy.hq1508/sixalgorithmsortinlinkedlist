import com.sun.org.apache.bcel.internal.generic.ARETURN;
import com.sun.org.apache.xpath.internal.objects.XNull;

import java.util.Date;
import java.util.Scanner;
import java.util.Timer;

public class SortLinkedList {
    // main method of program
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("-------------------Chuong Trinh 6 Thuat Toan Sap Xep Linked List------------------");
        boolean quit = true;
        do {
            System.out.print("Nhap vao so phan tu can sap xep: ");
            int n = scanner.nextInt();
            scanner.nextLine();
            Node node = initNode(n);

            System.out.println("1. Bubble Sort");
            System.out.println("2. Selection Sort");
            System.out.println("3. Insert Sort");
            System.out.println("4. Merge Sort");
            System.out.println("5. Quick Sort");
            System.out.println("6. Heap Sort");
            System.out.println("7. Quit");
            System.out.println("---------------------------------------------");
            System.out.println("Nhap vao lua chon cua ban");
            int choose = scanner.nextInt();
            scanner.nextLine();
            switch (choose) {
                case 1: {
                    System.out.println("Before sort !!!");
                    showNodeValue(node);
                    System.out.println("after sort !!! \n");
                    long startTime = new Date().getTime();
                    node = bubbleSort(node);
                    long endTime = new Date().getTime();
                    showNodeValue(node);
                    System.out.println("Thoi gian thuc thi cua bubble sort la: " + (double)(endTime - startTime) / 1000.0);
                    break;
                }
                case 2: {
                    System.out.println("Before sort !!!");
                    showNodeValue(node);
                    System.out.println("after sort !!! \n");
                    long startTime = new Date().getTime();
                    node = selectionSort(node);
                    long endTime = new Date().getTime();
                    showNodeValue(node);
                    System.out.println("Thoi gian thuc thi cua selection sort la: " + (double)(endTime - startTime) / 1000.0);
                    break;
                }
                case 3: {
                    System.out.println("Before sort !!!");
                    showNodeValue(node);
                    System.out.println("after sort !!! \n");
                    long startTime = new Date().getTime();
                    node = insertionSort(node);
                    long endTime = new Date().getTime();
                    showNodeValue(node);
                    System.out.println("Thoi gian thuc thi cua insertion sort la: " + (double)(endTime - startTime) / 1000.0);
                    break;
                }
                case 4: {
                    System.out.println("Before sort !!!");
                    showNodeValue(node);
                    System.out.println("after sort !!! \n");
                    long startTime = new Date().getTime();
                    node = mergeSort(node);
                    long endTime = new Date().getTime();
                    showNodeValue(node);
                    System.out.println("Thoi gian thuc thi cua merge sort la: " + (double)(endTime - startTime) / 1000.0);
                    break;
                }
                case 5: {
                    System.out.println("Before sort !!!");
                    showNodeValue(node);
                    System.out.println("after sort !!! \n");
                    long startTime = new Date().getTime();
                    quickSort(node, lastNode(node));
                    long endTime = new Date().getTime();
                    showNodeValue(node);
                    System.out.println("Thoi gian thuc thi cua quick sort la: " + (double)(endTime - startTime) / 1000.0);
                    break;
                }
                case 6: {
                    System.out.println("Before sort !!!");
                    showNodeValue(node);
                    System.out.println("after sort !!! \n");
                    long startTime = new Date().getTime();
                    heapSort(node);
                    long endTime = new Date().getTime();
                    showNodeValue(node);
                    System.out.println("Thoi gian thuc thi cua heap sort la: " + (double)(endTime - startTime) / 1000.0);
                    break;
                }
                case 7: {
                    System.out.println("Chao tam biet!!!!!!!!11");
                    quit = false;
                }
                default: {
                    System.out.println("Nhap sai roi! Vui long nhap lai");
                    break;
                }
            }
        } while (quit);
    }
    // method using for init the list
    public static Node initNode(int n){

        Node startNode = new Node();
        int value = (int) (Math.random() * n);
        startNode.setValue(value);
        Node prev = startNode;
        for(int i = 0; i < n ; i ++){
            Node node = new Node();
            value = (int) (Math.random() * n);
            node.setValue(value);
            prev.setNextNode(node);
            prev = node;
        }
        return startNode;
    }
    // method using for print list
    public static void showNodeValue(Node node){
        Node currentNode = node;

        while(currentNode.getNextNode() != null){
            System.out.print(currentNode .getValue()  + " <- ");
            currentNode = currentNode.getNextNode();
        }
        System.out.println(currentNode.getValue());
    }
    // ---------------------------------bubble sort ------------------------------------
    // bubble sort build in single linked list
    public static Node bubbleSort(Node node){
        // node header point to head of list node
        Node header = node;
        // if list has been sort => not sort
        boolean isChanged = true;
        // Exist more than 2 node
        if(node != null && node.getNextNode() != null){
            int i = 0;
            while(isChanged) {
                Node prevNode = null;
                Node current = header;
                Node nextNode = current.getNextNode();
                isChanged = false;
                while (!current.isLast()) {
                    // change node if value of pre node more than current node
                    if (current.getValue() > nextNode.getValue()) {
                        // if current node is header change header
                        if (header == current) {
                            header = nextNode;
                        } else {
                            prevNode.setNextNode(nextNode);
                        }
                        // change point of node
                        current.setNextNode(nextNode.getNextNode());
                        nextNode.setNextNode(current);
                        isChanged = true;
                        current = nextNode;
                    }
                    // set up locate of node
                    prevNode = current;
                    current = prevNode.getNextNode();
                    nextNode = current.getNextNode();
                }
            }

        }
        // return header of node
        return header;
    }
    // --------------------------------------- selection sort ------------------------------------------
    public static Node selectionSort(Node node) {
        Node header = node;
        Node preNodeOne, nodeOne, preNodeTwo, nodeTwo, nodeMin, preNodeMin;
        // preNodeOne and nodeOne point to header of list

        for (preNodeOne = nodeOne = header; !nodeOne.isLast(); preNodeOne = nodeOne, nodeOne = nodeOne.getNextNode()) {
            preNodeMin = nodeMin = nodeOne;

            for (preNodeTwo = nodeTwo = nodeOne.getNextNode(); nodeTwo != null; preNodeTwo = nodeTwo, nodeTwo = nodeTwo.getNextNode()) {
                if (nodeMin.getValue() > nodeTwo.getValue()) {
                    preNodeMin = preNodeTwo;
                    nodeMin = nodeTwo;
                }
            }

            // nodeOne is header list => change header of list
            if (nodeOne == header) {
                header = nodeMin;
                // nodeMin immediately nodeOne
                if (nodeMin == nodeOne.getNextNode()) {
                    nodeOne.setNextNode(nodeMin.getNextNode());
                    nodeMin.setNextNode(nodeOne); // *
                // nodeMin far nodeOne
                } else {
                    Node temp = nodeOne.getNextNode();
                    nodeOne.setNextNode(nodeMin.getNextNode());
                    nodeMin.setNextNode(temp);
                    preNodeMin.setNextNode(nodeOne);
                }

            } else {
                if (nodeMin == nodeOne.getNextNode()) {
                    preNodeOne.setNextNode(nodeMin);
                    nodeOne.setNextNode(nodeMin.getNextNode());
                    nodeMin.setNextNode(nodeOne);
                } else {
                    Node temp = nodeOne.getNextNode();
                    nodeOne.setNextNode(nodeMin.getNextNode());
                    preNodeMin.setNextNode(nodeOne);
                    preNodeOne.setNextNode(nodeMin);
                    nodeMin.setNextNode(temp);
                }

            }
            nodeOne = nodeMin;
        }
        return header;
    }
    // -----------------------------------insertion sort ----------------------------------
    public static Node insertionSort(Node node) {
        Node header = node;
        Node preNodeOne, nodeOne, preNodeTwo, nodeTwo;

        for (preNodeOne = nodeOne = header.getNextNode(); nodeOne != null; preNodeOne = nodeOne, nodeOne = nodeOne.getNextNode()) {
            for (preNodeTwo = nodeTwo = header; nodeTwo != nodeOne; preNodeTwo = nodeTwo, nodeTwo = nodeTwo.getNextNode()) {
                // Find value in pre nodeOne greater than nodeOne
                if (nodeTwo.getValue() > nodeOne.getValue()) {
                    Node temp = nodeOne.getNextNode();
                    // nodeTwo is header of list

                    if (nodeTwo == header) {
                        // update header of list
                        // change node if nodeOne next nodeTwo
                        if (nodeTwo.getNextNode() == nodeOne) {
                            nodeTwo.setNextNode(temp);
                            nodeOne.setNextNode(nodeTwo);
                            // update locate nodeOne
                            header = nodeOne;
                            nodeOne = nodeTwo;

                        // change node if nodeOne far nodeTwo
                        } else {
                            preNodeOne.setNextNode(temp);
                            nodeOne.setNextNode(nodeTwo);
                            // update locate nodeOne
                            header = nodeOne;
                            nodeOne = preNodeOne;
                        }

                    } else { // in case nodeTwo is not header
                        //  change node if nodeOne next nodeTwo
                        if (nodeTwo.getNextNode() == nodeOne) {
                            preNodeTwo.setNextNode(nodeOne);
                            nodeOne.setNextNode(nodeTwo);
                            nodeTwo.setNextNode(temp);
                            // update locate nodeOne

                            nodeOne = nodeTwo;

                        } else { // change node if nodeOne far nodeTwo
                            preNodeOne.setNextNode(temp);
                            preNodeTwo.setNextNode(nodeOne);
                            nodeOne.setNextNode(nodeTwo);
                            // update locate nodeOne
                            nodeOne = preNodeOne;
                        }
                    }
                    // If find right locate break to nextNodeOne
                    break;
                }
            }
        }
        return header;
    }
    // ------------------------------------- merge sort ---------------------------------------
    public static Node mergeSort(Node node) {
        if (node == null || node.getNextNode() == null) {
            return node;
        }

        Node middle = getMiddle(node);
        Node nextOfMiddle = middle.getNextNode();

        middle.setNextNode(null);

        Node left = mergeSort(node);
        Node right = mergeSort(nextOfMiddle);
        Node sorted = merge(left, right);
        return sorted;
    }

    public static Node merge(Node left, Node right) {
        Node result = null;
        if (left == null) {
            return right;
        }
        if (right == null) {
            return left;
        }

        // pick headOne or headTwo to result
        if (left.getValue() <= right.getValue()) {
            result = left;
            result.setNextNode(merge(left.getNextNode(), right));
        } else {
            result = right;
            result.setNextNode(merge(left, right.getNextNode()));
        }
        return result;
    }

    public static Node getMiddle(Node node) {
        // base case
        if (node == null) {
            return node;
        }

        Node fastNode = node.getNextNode();
        Node slowNode = node;

        // move fastNode by two and slowNode by one
        // finally showNode will point to middle node
        while (fastNode != null) {
            fastNode = fastNode.getNextNode();
            if (fastNode != null) {
                fastNode = fastNode.getNextNode();
                slowNode = slowNode.getNextNode();
            }
        }
        return slowNode;
    }

    // --------------------------------quick sort -------------------------------------------
    public static Node partitionLast(Node start, Node end) {
        if (start == end || start == null || end == null) {
            return start;
        }

        Node pivot_pre = start;
        Node curr = start;
        int pivot = end.getValue();

        while (start.getNextNode() != end) {
            if (start.getValue() < pivot) {
                pivot_pre = curr;
                int temp = curr.getValue();
                curr.setValue(start.getValue());
                start.setValue(temp);
                curr = curr.getNextNode();
            }

            start = start.getNextNode();
        }

        int temp = curr.getValue();
        curr.setValue(pivot);
        end.setValue(temp);

        return pivot_pre;

    }

    public static void quickSort(Node start, Node end) {
        if (start == end) {
            return;
        }
        Node pivot_pre = partitionLast(start, end);
        quickSort(start, pivot_pre);
        // if pivot pickup and moved to the start. that mean start and pivot is same
        if (pivot_pre != null && pivot_pre == start) {
            quickSort(pivot_pre.getNextNode(), end);
        } else if (pivot_pre != null && pivot_pre.getNextNode() != null) {
            quickSort(pivot_pre.getNextNode().getNextNode(), end);
        }
    }

    public static Node lastNode(Node node) {
        Node last = node;
        while (last.getNextNode() != null) {
            last = last.getNextNode();
        }

        return last;
    }

    // ----------------------------------------heapsort in linked list -----------------------------
    public static Node returnNodeByIndex(Node node, int index) {
        Node nodeIndex = node;
        for (int i = 0; i < index; i++) {
            nodeIndex = nodeIndex.getNextNode();
        }
        return nodeIndex;
    }

    public static void heapify(Node node, int n, int i) {
        int largest = i; // init largest as root
        int l = 2 * i + 1;
        int r = 2 * i + 2;

        // if left child is larger than root
        if (l < n && returnNodeByIndex(node, l).getValue() > returnNodeByIndex(node, largest).getValue()) {
            largest = l;
        }
        // if right child is larger than root
        if (r < n && returnNodeByIndex(node, r).getValue() > returnNodeByIndex(node, largest).getValue()) {
            largest = r;
        }

        // if largest is not root
        if (largest != i) {
            int swap = returnNodeByIndex(node, i).getValue();
            returnNodeByIndex(node, i).setValue(returnNodeByIndex(node, largest).getValue());
            returnNodeByIndex(node, largest).setValue(swap);
            heapify(node, n, largest);
        }
    }

    public static int getLength(Node node) {
        Node temp = node;
        int length = 0;
        while (temp != null) {
            temp = temp.getNextNode();
            length++;
        }
        return length;
    }

    public static void heapSort(Node node) {
        int n = getLength(node);

        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(node, n, i);
        }
        System.out.print("Change --------------- ");
        showNodeValue(node);

        for (int i = n - 1; i >= 0; i--) {
            // move root to the end
            int swap = returnNodeByIndex(node, 0).getValue();
            returnNodeByIndex(node, 0).setValue(returnNodeByIndex(node, i).getValue());
            returnNodeByIndex(node, i).setValue(swap);
            heapify(node, i, 0);
        }
    }
}