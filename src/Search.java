import java.lang.reflect.Array;
import java.util.*;

public class Search {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhap vao so phan tu: ");
        int length = scanner.nextInt();

        int arr[] = new int[length];
        for (int i = 0; i < length; i++) {
            int data = (int) (Math.random() * length);
            arr[i] = data;
        }
        Arrays.sort(arr);

        for (int i = 0; i < length; i++) {
            System.out.print(arr[i] + " ");
        }

        int index_search = linearSearch(arr, 50);
        System.out.println("\nDa tim thay 50 tai vi tri: " + index_search);
        index_search = binarySearchRecursion(arr, 0, length, 50);
        System.out.println("\nDa tim thay 50 tai vi tri: " + index_search);
        index_search = binarySearchNomal(arr, 0, length, 50);
        System.out.println("\nDa tim thay 50 tai vi tri: " + index_search);
    }

    public static int linearSearch(int arr[], int data) {
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            if (arr[i] == data) {
                return i + 1;
            }
        }
        return -1;
    }

    public static int binarySearchRecursion(int arr[], int left, int right, int data) {
        if (left <= right) {
            int mid = (left + right) / 2;

            if (data == arr[mid]) {
                return mid + 1;
            } else if (data < arr[mid]) {
                return binarySearchRecursion(arr, left, mid - 1, data);
            } else {
                return binarySearchRecursion(arr, mid + 1, right, data);
            }

        }
        return -1;
    }

    public static int binarySearchNomal(int arr[], int left, int right, int data) {
        while (left <= right) {
            int mid = (left + right) / 2;
            if (arr[mid] == data) {
                return mid + 1;
            }

            if (arr[mid] > data) {
                right = mid - 1;
            }

            if (arr[mid] < data) {
                left = mid + 1;
            }
        }

        return -1;
    }

}
